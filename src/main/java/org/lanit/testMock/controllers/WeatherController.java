package org.lanit.testMock.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

@Controller
public class WeatherController {
    final static List<String> CITIES = Arrays.asList("Москва", "Санкт-Петербург", "Нижний Новгород", "Саратов");
    final static List<String> DESCRIPTION = Arrays.asList("Ясно", "Переменная облачность", "Гроза", "Возможен дождь");
    @GetMapping("/weather")
    public ResponseEntity<String> getWeather() {
        Random random = new Random();
        int temperature = random.nextInt(10) + 15;
        int humidity = random.nextInt(89) + 10;
        int cityIndex = random.nextInt(CITIES.size());
        int descriptionIndex = random.nextInt(DESCRIPTION.size());

        String responseBody = String.format("{%n  \"city\": \"%s\",%n  \"temperature\": %d,%n  \"description\": \"%s\",%n  \"humidity\": %d%n}",
                CITIES.get(cityIndex), temperature, DESCRIPTION.get(descriptionIndex), humidity);

        return ResponseEntity
                .ok()
                .header("content-type", "application/json")
                .body(responseBody);
    }
}
