package org.lanit.testMock.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class StatusController {
    @GetMapping("/status")
    public ResponseEntity<String> getStatus() {
        String responseBody = """
                {
                  "status": "ok",
                  "message": "Сервис работает в тестовом режиме."
                }""";
        return ResponseEntity
                .ok()
                .header("content-type", "application/json")
                .body(responseBody);
    }
}