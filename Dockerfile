FROM openjdk:17.0.1-jdk-slim
WORKDIR /opt/app
COPY target/testMock-1.0.jar testMock.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "testMock.jar"]